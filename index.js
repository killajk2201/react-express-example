const express = require('express');
const path = require('path');
const fs = require('fs');
const bodyparser = require('body-parser');
const uuidv1 = require('uuid/v1');
const PORT = process.env.PORT || 5000;

var app = express();
var jsonstring = fs.readFileSync('./students.json');
var students = JSON.parse(jsonstring);

app.use('/public', express.static(path.join(__dirname, 'public')));
app.use('/static', express.static(path.join(__dirname, 'client/build/static')));
app.use('/', express.static(path.join(__dirname, 'client/build')));
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));
app.get('/api/students', (req, res, next) => {
	console.log('GET request for: /students');
	res.setHeader('Content-Type', 'application/json');
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.send(JSON.stringify(students));
	res.end();
});
app.put('/api/students/add', (req, res, next) => {
	console.log('PUT request for: /students/add');
	req.body.newStudent.id = uuidv1();
	console.log(req.body.newStudent);
	console.log(typeof req.body.newStudent);
	students.push(req.body.newStudent);
	let response = '';
	fs.writeFile('./students.json', JSON.stringify(students), err => {
		if (err) {
			response = { code: 500, message: 'Unable to save server file' };
			console.log(err);
		} else {
			response = { code: 200, message: 'Successfully added a new student' };
			console.log('Saved new student to students.json');
		}
	});
	console.log(response);
	res.setHeader('Content-Type', 'application/json');
	res.json(response);
	res.end();
});
app.get('/api/students/convert', (req, res, next) => {
	students.forEach((student, ind, arr) => {
		student.id = uuidv1();
	});
	fs.writeFile('./students.json', JSON.stringify(students), err => {
		if (err) {
			console.log(err);
		}
	});
	res.end();
});
app.delete('/api/students/:id', (req, res, next) => {
	students = students.filter((student, index, arr) => {
		if (student.id === req.params.id) {
			return false;
		} else {
			return true;
		}
	});
	fs.writeFile('./students.json', JSON.stringify(students), err => {
		if (err) {
			console.log(err);
		}
	});
	res.send(`Successfully deleted student with id: ${req.params.id}`);
	res.end();
});

app.get('/', (req, res) => {
	res.sendFile(path.join(__dirname, '/client/build/index.html'));
});
app.listen(PORT, () => console.log(`Server Listening on Port: ${PORT}`));
